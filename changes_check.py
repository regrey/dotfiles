#!/usr/bin/env python3

import getpass
import subprocess

PATH_TRACK_FILES = "readme"
USER_NAME = getpass.getuser()


def get_tracked_files(path_for_track_files):
    with open(path_for_track_files, "r") as file:
        raw_lines = file.readlines()

    tracked_files = {}
    for raw_line in raw_lines:
        if raw_line.find("=") != -1:
            name, value = raw_line.split("=", maxsplit=1)
            tracked_files.update({name : value.strip()})
    return tracked_files


def get_changed_files(tracked_files):
    answer = ""
    for key in tracked_files.keys():
        value = tracked_files[key].replace("$USER", USER_NAME)
        process = subprocess.Popen(["diff", "-q", key, value], stdout=subprocess.PIPE)
        output, _ = process.communicate()
        answer = answer + output.decode("UTF-8")
    return answer


def main():
    tracked_files = get_tracked_files(PATH_TRACK_FILES)
    changed_files = get_changed_files(tracked_files)
    print(changed_files)


if __name__ == '__main__':
    main()
