#!/usr/bin/env python3

import sys

ORIG_PATH = "/usr/lib/python3.9/site-packages/libqtile/widget"
sys.path.append(ORIG_PATH)

from volume import Volume

class CustomVolume(Volume):

    pass
    #def _update_drawer(self):

    #    orig_volume = int(self.volume)
    #    self.text = orig_volume
    #    additional_number = 0
    #    if orig_volume > 100:
    #        additional_number = int(orig_volume%100)
    #    normalized_volume = int((orig_volume - additional_number)/2) + additional_number

    #    if normalized_volume <= 0:
    #        self.text = '   '
    #    #elif normalized_volume == 0:
    #    #    self.text = '00%'
    #    elif normalized_volume < 50:
    #        self.text = '{:0>2d}%'.format(normalized_volume)
    #    elif normalized_volume <= 100:
    #        self.text = '{}%'.format(normalized_volume)
    #    else:
    #        self.text = '[custom volume error!!!]'
