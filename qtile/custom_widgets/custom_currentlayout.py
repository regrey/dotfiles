#!/usr/bin/env python3

import sys

ORIG_PATH = "/usr/lib/python3.9/site-packages/libqtile/widget"
sys.path.append(ORIG_PATH)

from currentlayout import CurrentLayoutIcon

class CustomCurrentLayoutIcon(CurrentLayoutIcon):

    def __init__(self, **config):
        self.scale = 0.6
        CurrentLayoutIcon.__init__(self, **config)

