#!/usr/bin/env python3

import sys

ORIG_PATH = "/usr/lib/python3.9/site-packages/libqtile/widget"
sys.path.append(ORIG_PATH)

from cpu import CPU
from memory import Memory

class CustomCPU(CPU):
    defaults = [
        ("update_interval", 1.0, "Update interval for the CPU widget"),
        (
            "format",
            " CPU {load_percent}%",
            "CPU display format",
        ),
    ]

    def __init__(self, **config):
        super().__init__(**config)
        self.add_defaults(CustomCPU.defaults)

class CustomMemory(Memory):
    defaults = [
        ("format",
        " {MemUsed}Mb",
        "Formatting for field names."),
        ("update_interval", 1.0, "Update interval for the Memory"),
    ]

    def __init__(self, **config):
        super().__init__(**config)
        self.add_defaults(CustomMemory.defaults)
