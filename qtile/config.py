# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile.config import KeyChord, Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

import sys
sys.path.insert(0, "/home/sergey/.config/qtile/custom_widgets/")
from custom_volume import CustomVolume 
from custom_currentlayout import CustomCurrentLayoutIcon
from custom_hardmonitor import CustomCPU, CustomMemory

mod = "mod4"                                        # Sets mod key to SUPER/WINDOWS
myTerm = "xterm"                                # My terminal of choice 
myPath = "/home/sergey/.config/qtile/"
myConfig = myPath + "config.py"   # The Qtile config file location

color = {   "bar_background_main": "101010",
            "bar_background_additional": "0FB6E7",
            "bar_font_active": "E7FDFF",
            "bar_font_innactive": "596164",
            "bar_font_warn": "4B0D0D",
            "border_focus": "AA0000",
            "border_normal": "000000"
         }

keys = [
    Key([mod], "Return", lazy.spawn(myTerm), desc="Launch terminal"),
    Key([mod], "d", lazy.spawn('dmenu_run'), desc="Launch dmenu"),
    Key([mod], "e", lazy.spawn("firefox"), desc="Launch firefox"),
    Key([mod], "m", lazy.spawn("thunderbird"), desc="Launch thunderbird"),
    Key([mod], "f", lazy.spawn("thunar"), desc="File manager"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    Key([], 'Print', lazy.spawn("flameshot gui"), desc="Screenshot full screen"),
    #Key(["control"], 'Print', lazy.spawn("shutter -s"), desc="Screenshot selection"),
    Key(["shift"], 'Print', lazy.spawn("flameshot full -p /home/sergey/Pictures/screenshots/"), desc="Screenshot full screen"),

    Key([mod], "space", lazy.spawn("/home/sergey/scripts/qtile_change_keyboard"), desc="Language switch"),

    Key([mod], "F12", lazy.spawn("/home/sergey/scripts/volume_plus"), desc="Volume increase"),
    Key([mod], "F11", lazy.spawn("/home/sergey/scripts/volume_minus"), desc="Volume degrease"),
    Key([mod], "F10", lazy.spawn("amixer -q sset Master toggle"), desc="Volume on/off"),
    Key([mod], "F9", lazy.spawn("/home/sergey/scripts/volume_source_switch"), desc="Volume switch source"),

    # Change modes for multimonitors modes
    Key([mod, "control"], "Down", lazy.spawn("xrandr --output eDP-1 --auto"), desc="Duplicate screens"), #FIXME add the notification
    Key([mod, "control"], "Up", lazy.spawn("xrandr --output eDP-1 --off"), desc="Only extended monitor"), #FIXME add the notification
    Key([mod, "control"], "Left", lazy.spawn("xrandr --output eDP-1 --mode 1920x1080 --right-of HDMI-1"), desc="Extended monitor in left"), #FIXME add the notification
    # TODO xrandr can also manages the birghtess

    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(), desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(), desc="Move focus up in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "shift"], "k", lazy.layout.shuffle_down(), desc="Move window down in current stack "),
    Key([mod, "shift"], "j", lazy.layout.shuffle_up(), desc="Move window up in current stack "),

    # Switch window focus to other pane(s) of stack
    Key([mod], "Tab", lazy.layout.next(), desc="Switch window focus to other pane(s) of stack"),
    Key(["mod1"], "Tab", lazy.group.next_window(), desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([mod, "shift"], "Tab", lazy.layout.rotate(), desc="Swap panes of split stack"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),

    # Toggle between different layouts as defined below
    Key(["mod1"], "space", lazy.next_layout(), desc="Toggle between layout modes"),
    Key(["mod1"], "F4", lazy.window.kill(), desc="Kill focused window"), #FIXME delete it as duplicate
    Key([mod], "F4", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "w", lazy.window.kill(), desc="Kill focused window"), #FIXME duplicate

    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile config"),
    Key([mod, "control"], "l", lazy.shutdown(), desc="Shutdown qtile"),

     ### Switch focus to specific monitor (out of three)
     Key([mod], "q", lazy.to_screen(0), desc='Keyboard focus to monitor 1'),
     Key([mod], "w", lazy.to_screen(1), desc='Keyboard focus to monitor 2'),
     ### Switch focus of monitors
     Key([mod], "period", lazy.next_screen(), desc='Move focus to next monitor'),
     Key([mod], "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),
]

group_names = [("", {'layout': 'monadtall'}),  #WWW
               ("", {'layout': 'max'}),        #TERM
               ("", {'layout': 'monadtall'}),  #CHAT
               ("", {'layout': 'max'}),        #DEV
               ("", {'layout': 'floating'}),   #GAMES
               ("", {'layout': 'monadtall'})]  #OTHER

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 4,
                "border_focus" : color["border_focus"],
                "border_normal": color["border_normal"],
                }

layouts = [
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    layout.Stack(num_stacks=2),
    #layout.TreeTab(
    #     font = "Ubuntu",
    #     fontsize = 10,
    #     sections = ["FIRST", "SECOND"],
    #     section_fontsize = 11,
    #     bg_color = "141414",
    #     active_bg = "90C435",
    #     active_fg = "000000",
    #     inactive_bg = "384323",
    #     inactive_fg = "a0a0a0",
    #     padding_y = 5,
    #     section_top = 10,
    #     panel_width = 320
    #     ),
    layout.Floating(**layout_theme)
]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=2,
    background=color["bar_background_main"]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
        widget.GroupBox(
            active=color["bar_font_active"],
            inactive=color["bar_font_innactive"],
            this_current_screen_border=color["border_focus"],
        ),
        widget.Sep(padding=4, linewidth=0),
        CustomCurrentLayoutIcon(),
        #widget.CurrentLayout(),
        widget.Sep(padding=4, linewidth=0),
        widget.Prompt(),
        widget.WindowName(),
        widget.Notify(),
        widget.Sep(padding=4, linewidth=0),
        CustomCPU(),
        widget.Sep(padding=4, linewidth=0),
        CustomMemory(),
        widget.Sep(padding=4, linewidth=0),
        #widget.Net(interface="wlp3s0", foreground='444444', format='{interface}: {down} ↓↑ {up}'),
        widget.Sep(padding=4, linewidth=0),
        #widget.CheckUpdates(),
        widget.TextBox(""),
        widget.KeyboardLayout(),
        widget.Sep(padding=4, linewidth=0),
        widget.TextBox(""),
        #widget.BatteryIcon(),
        widget.Battery(charge_char='+', discharge_char='', format='{char}{percent:2.0%}'),
        #widget.Chord(
        #    chords_colors={
        #        'launch': ("#ff0000", "#ffffff"),
        #    },
        #    name_transform=lambda name: name.upper(),
        #),
        widget.Sep(padding=4, linewidth=0),
        CustomVolume(),
        widget.Sep(padding=8, linewidth=0),
        widget.Systray(background='222222', icon_size=14),
        widget.Sep(padding=8, linewidth=0),
        widget.Clock(format=' %Y-%m-%d [%a] %H:%M'),
    ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1                       # Slicing removes unwanted widgets on Monitor 1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                       # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20),
            wallpaper="/home/sergey/Pictures/wallpapers/sunset_winter_trees_landscape_1920x1080.jpg"
            ),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20),
            wallpaper="/home/sergey/Pictures/wallpapers/sunset_winter_trees_landscape_1920x1080.jpg"
            )]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

# Drag floating layouts.
mouse = [
    Drag(["mod1"], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag(["mod1"], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click(["mod1"], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'MyGdxGame'},  # MyGdxGame Desktop window
    {'wname': 'Android Emulator'},  # Android emulator (need test it)
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


# Autostart programms
@hook.subscribe.startup_once
def autostart():
    processes = [
        ['picom'],
        ['numlockx'],
        ['flameshot'],
        ['optimus-manager-qt']
    ]

    for p in processes:
        subprocess.Popen(p)

