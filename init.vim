set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc

" :source ~/.vimrc
map <Leader>r :source ~/.config/nvim/init.vim<CR>

nmap <F4> :source ~/.config/nvim/init.vim<CR>
nmap <F8> :TagbarToggle<CR>
nmap <silent> ;i :IndentLinesToggle<CR>

set number
set laststatus=2

" Color current line
set cursorline
hi CursorLine term=bold cterm=bold guibg=Grey40
"hi CursorLine guibg=#222222 gui=none
hi CursorLineNr cterm=None term=bold ctermfg=Yellow gui=bold guifg=Yellow

"TERM=xterm-256color

"===============for yaml==============================
" https://www.arthurkoziel.com/setting-up-vim-for-yaml/
"---apt install yamllint---
"set foldlevelstart=20
"
"let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
"let g:ale_sign_error = '✘'
"let g:ale_sign_warning = '⚠'
"let g:ale_lint_on_text_changed = 'never'

"""In ~/.config/yamllint/config
"  extends: relaxed
"  rules:
"    line-length: disable

"---------------------------

"=====================================================
" Vundle settings
"=====================================================
" for setup run ':PluginInstall'
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'		" let Vundle manage Vundle, required
Plugin 'itchyny/lightline.vim'  " Lightline statusbar
Plugin 'rbgrouleff/bclose.vim'
Plugin 'davidhalter/jedi-vim' 	" Jedi-vim autocomplete plugin
Plugin 'majutsushi/tagbar' 		" tagbar F8
Plugin 'Shougo/denite.nvim'     " FIXME it doesn't work yet (I hope)
Plugin 'preservim/nerdtree'     " See NerdTree section

call vundle#end()            		" required
filetype on
filetype plugin on
filetype plugin indent on
"===================NerdTree======================https://github.com/preservim/nerdtree
nmap <F5> :NERDTreeToggle<CR>
"===================INDENDT LINE======================https://github.com/Yggdroot/indentLine
Plugin 'Yggdroot/indentLine'
let g:indentLine_char_list = ['¦']
"=================== JEDI-VIM ========================
" Disable choose first function/method at autocomplete
let g:jedi#force_py_version = 3
let g:jedi#popup_select_first = 0
let g:jedi#popup_on_dot = 0

" Popup window with function signature
" let g:jedi#show_call_signatures = 0
let g:jedi#show_call_signatures_delay = 2500

let g:pymode_rope = 0
"=====================================================
" VIM calc. Example 8+8 CTRL+A (in insert mode)
"ino <C-A> <C-O>yiW<End>=<C-R>=<C-R>0<CR>

