" :source ~/.vimrc
map <Leader>r :source ~/.vimrc<CR>

set tabstop=4
set expandtab
set shiftwidth=4
set smarttab
nnoremap * *N
set ignorecase
set backspace=indent,eol,start
"set number
set laststatus=0
"set mouse=a

" russinal language support
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz

" copy to clipboard
set clipboard=unnamedplus

" when delete no copy (not work)
" nnoremap <leader>d "_d
" xnoremap <leader>d "_d
" xnoremap <leader>p "_dP

nmap <silent> ;n :call NumberHand()<CR>
nmap <silent> ;h :set hls!<CR>
nmap <silent> ;l :set spell! spelllang=en_us<CR>
nmap <silent> ;s :call LastStatus()<CR>

nmap <F1> :tabprev<CR>
nmap <F2> :tabnext<CR>
nmap <F3> :tabnew<CR>

function! NumberHand()
    if &number == "0"
        set number
    else
        if &relativenumber == "0"
            set relativenumber
        else
            set norelativenumber
            set nonumber
        endif
    endif
endfunc

function! LastStatus()
    if &laststatus == "laststatus=0"
        set laststatus=2
    else
        set laststatus=0
    endif
endfunc

" moving lines virtically
"xnoremap K :move '<-2<CR>gv=gv
"xnoremap J :move '>+1<CR>gv=gv

" Settings for buffers
noremap <C-Right> :bn<CR> " move to next buffer
noremap <C-Left> :bp<CR>  " move to previous buffer

" autocmd Filetype javascript setlocal ts=2 sw=2 sts=0 expandtab
autocmd Filetype html setlocal ts=2 sw=2 sts=0 expandtab
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=2 shiftwidth=2
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set number

set path+=**
set wildmenu

set encoding=utf-8

if has('nvim')
   "something for neovim
else
    " rigth color
    set term=xterm-256color
endif

" work with ctags inside vim
set tags=tags

colorscheme delek

set nocompatible
filetype off

" FIXME
"if has("syntax")
"  syntax on
"endif

" Color tab bar
hi TabLineFill ctermfg=DarkGrey ctermbg=DarkGrey
hi TabLine ctermfg=LightGrey ctermbg=DarkGrey
hi TabLineSel ctermfg=LightGrey ctermbg=Black

"TEST FOR COLLORING
"highlight Test ctermfg=red
"autocmd BufWinEnter * match Test /\[\-\]/
"autocmd InsertEnter * match Test /\[\-\]/
"autocmd InsertLeave * match Test /\[\-\]/
"autocmd BufWinLeave * call clearmatches()

